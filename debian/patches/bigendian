commit 07af274f6dc8cffde2a76ba543f5d0b0ad3ea0a5
Author: Christoph Berg <myon@debian.org>
Date:   Wed Sep 6 15:20:55 2023 +0000

    Fix PG protocol handling on big-endian
    
    Replace the homegrown swap-bytes machinery by system functions that work
    correctly independently from the host byte order. pgagroal_read/write_*
    were wrong on big-endian machines before.

--- a/src/include/utils.h
+++ b/src/include/utils.h
@@ -166,21 +166,6 @@ void
 pgagroal_write_string(void* data, char* s);
 
 /**
- * Is the machine big endian ?
- * @return True if big, otherwise false for little
- */
-bool
-pgagroal_bigendian(void);
-
-/**
- * Swap
- * @param i The value
- * @return The swapped value
- */
-unsigned int
-pgagroal_swap(unsigned int i);
-
-/**
  * Print the available libev engines
  */
 void
--- a/src/libpgagroal/security.c
+++ b/src/libpgagroal/security.c
@@ -3937,7 +3937,7 @@ salted_password(char* password, char* sa
 {
    size_t size = 32;
    int password_length;
-   unsigned int one;
+   uint32_t one = htobe32(1);
    unsigned char Ui[size];
    unsigned char Ui_prev[size];
    unsigned int Ui_length;
@@ -3958,15 +3958,6 @@ salted_password(char* password, char* sa
 
    password_length = strlen(password);
 
-   if (!pgagroal_bigendian())
-   {
-      one = pgagroal_swap(1);
-   }
-   else
-   {
-      one = 1;
-   }
-
    r = calloc(1, size);
 
    /* SaltedPassword: Hi(Normalize(password), salt, iterations) */
--- a/src/libpgagroal/utils.c
+++ b/src/libpgagroal/utils.c
@@ -34,6 +34,7 @@
 
 /* system */
 #include <ev.h>
+#include <endian.h>
 #include <execinfo.h>
 #include <pwd.h>
 #include <stdint.h>
@@ -287,53 +288,19 @@ pgagroal_read_byte(void* data)
 int16_t
 pgagroal_read_int16(void* data)
 {
-   unsigned char bytes[] = {*((unsigned char*)data),
-                            *((unsigned char*)(data + 1))};
-
-   int16_t res = (int16_t)((bytes[0] << 8)) |
-                 ((bytes[1]));
-
-   return res;
+   return be16toh(*((uint16_t *)data));
 }
 
 int32_t
 pgagroal_read_int32(void* data)
 {
-   unsigned char bytes[] = {*((unsigned char*)data),
-                            *((unsigned char*)(data + 1)),
-                            *((unsigned char*)(data + 2)),
-                            *((unsigned char*)(data + 3))};
-
-   int32_t res = (int32_t)((bytes[0] << 24)) |
-                 ((bytes[1] << 16)) |
-                 ((bytes[2] << 8)) |
-                 ((bytes[3]));
-
-   return res;
+   return be32toh(*((uint32_t *)data));
 }
 
 long
 pgagroal_read_long(void* data)
 {
-   unsigned char bytes[] = {*((unsigned char*)data),
-                            *((unsigned char*)(data + 1)),
-                            *((unsigned char*)(data + 2)),
-                            *((unsigned char*)(data + 3)),
-                            *((unsigned char*)(data + 4)),
-                            *((unsigned char*)(data + 5)),
-                            *((unsigned char*)(data + 6)),
-                            *((unsigned char*)(data + 7))};
-
-   long res = (long)(((long)bytes[0]) << 56) |
-              (((long)bytes[1]) << 48) |
-              (((long)bytes[2]) << 40) |
-              (((long)bytes[3]) << 32) |
-              (((long)bytes[4]) << 24) |
-              (((long)bytes[5]) << 16) |
-              (((long)bytes[6]) << 8) |
-              (((long)bytes[7]));
-
-   return res;
+   return be64toh(*((uint64_t *)data));
 }
 
 char*
@@ -351,37 +318,13 @@ pgagroal_write_byte(void* data, signed c
 void
 pgagroal_write_int32(void* data, int32_t i)
 {
-   char* ptr = (char*)&i;
-
-   *((char*)(data + 3)) = *ptr;
-   ptr++;
-   *((char*)(data + 2)) = *ptr;
-   ptr++;
-   *((char*)(data + 1)) = *ptr;
-   ptr++;
-   *((char*)(data)) = *ptr;
+   *((uint32_t *)data) = htobe32(i);
 }
 
 void
 pgagroal_write_long(void* data, long l)
 {
-   char* ptr = (char*)&l;
-
-   *((char*)(data + 7)) = *ptr;
-   ptr++;
-   *((char*)(data + 6)) = *ptr;
-   ptr++;
-   *((char*)(data + 5)) = *ptr;
-   ptr++;
-   *((char*)(data + 4)) = *ptr;
-   ptr++;
-   *((char*)(data + 3)) = *ptr;
-   ptr++;
-   *((char*)(data + 2)) = *ptr;
-   ptr++;
-   *((char*)(data + 1)) = *ptr;
-   ptr++;
-   *((char*)(data)) = *ptr;
+   *((uint64_t *)data) = htobe64(l);
 }
 
 void
@@ -390,23 +333,6 @@ pgagroal_write_string(void* data, char*
    memcpy(data, s, strlen(s));
 }
 
-bool
-pgagroal_bigendian(void)
-{
-   short int word = 0x0001;
-   char* b = (char*)&word;
-   return (b[0] ? false : true);
-}
-
-unsigned int
-pgagroal_swap(unsigned int i)
-{
-   return ((i << 24) & 0xff000000) |
-          ((i << 8) & 0x00ff0000) |
-          ((i >> 8) & 0x0000ff00) |
-          ((i >> 24) & 0x000000ff);
-}
-
 void
 pgagroal_libev_engines(void)
 {
